package com.spring.requestparams;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BasicSpringReqParamsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BasicSpringReqParamsApplication.class, args);
	}
}
